// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyTDProjectGameMode.generated.h"

UCLASS(minimalapi)
class AMyTDProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyTDProjectGameMode();
};



