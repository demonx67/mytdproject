// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyTDProjectGameMode.h"
#include "MyTDProjectPlayerController.h"
#include "MyTDProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AMyTDProjectGameMode::AMyTDProjectGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMyTDProjectPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}